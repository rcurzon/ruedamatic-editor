'use strict'

import { app, BrowserWindow, dialog, ipcMain } from 'electron'
import path from 'path'

/**
 * Set `__static` path to static files in production
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-static-assets.html
 */
if (process.env.NODE_ENV !== 'development') {
  global.__static = require('path').join(__dirname, '/static').replace(/\\/g, '\\\\')
}

let mainWindow
const winURL = process.env.NODE_ENV === 'development'
  ? `http://localhost:9080`
  : `file://${__dirname}/index.html`

function createWindow () {
  /**
   * Initial window options
   * skipTaskBar, toolbar added rc 20170806
   */
  mainWindow = new BrowserWindow({
    // origin RM: 1152x864.  most common gs.statcounter.com: 1366x768 1920x1080
    height: 864,
    'min-height': 864,
    useContentSize: true, // true
    width: 1152, // 1000 1366 1152 1280
    'min-width': 1152,
    skipTaskbar: false,
    toolbar: false,
    webPreferences: { // https://stackoverflow.com/questions/48777336/is-disabling-websecurity-on-the-electron-window-the-only-way-to-display-local-im
      webSecurity: process.env.NODE_ENV !== 'development'
    }
  })

  mainWindow.setMenu(null) // added rc 20170806
  mainWindow.loadURL(winURL)

  mainWindow.on('close', e => {
    console.log('closing...')
    let choice = dialog.showMessageBox(
      mainWindow,
      {
        type: 'question',
        buttons: ['OK', 'Cancel'],
        title: 'Quit RuedaMatic Editor?',
        message: 'Sure you want to quit now?'
      }
    )
    // index of clicked button (1 : No)
    if (choice === 1) e.preventDefault()
  })

  mainWindow.on('closed', () => {
    mainWindow = null
  })
}
console.log('main: setting up ipcMain listener')
ipcMain.on('asynch-md5-request', (event, filename) => {
  console.log('debug asynch request: ' + filename)
  console.log('IPC sending on channel asynch-md5-result')
  let child = require('child_process').execFile
  // source: https://www.npmjs.com/package/ffmpeg-static
  /* eslint-disable no-undef */ // for __static, from webpack
  let execPath = path.join(__static, '/ffmpegLocal.exe')
  /* eslint-enable no-undef */
  let params = ['-i', filename, '-vn', '-f', 'md5', '-']

  child(execPath, params, (error, stdout, stderr) => {
    if (error) {
      console.log(`exec error: ${error}`)
      console.error(`exec error: ${error}`)
    }
    console.log(`stdout: ${stdout}`)
    console.log(`stderr: ${stderr}`)
    console.log('event sender send is: ' + event.sender.send)
    // ret string is like 'MD5=F123....' so split off the value
    event.sender.send('asynch-md5-result', stdout.split('=')[1].trim())
    console.log('debug asynch result: ' + stdout)
  })

  // Synchronous event emmision
})

app.on('ready', createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
  }
})

/**
 * Auto Updater
 *
 * Uncomment the following code below and install `electron-updater` to
 * support auto updating. Code Signing with a valid certificate is required.
 * https://simulatedgreg.gitbooks.io/electron-vue/content/en/using-electron-builder.html#auto-updating
 */

/*
import { autoUpdater } from 'electron-updater'

autoUpdater.on('update-downloaded', () => {
  autoUpdater.quitAndInstall()
})

app.on('ready', () => {
  if (process.env.NODE_ENV === 'production') autoUpdater.checkForUpdates()
})
 */
