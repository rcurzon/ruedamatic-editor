import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'landing-page',
      component: require('@/components/LandingPage')
    },
    {
      path: '/moves',
      name: 'moves',
      component: require('@/components/Moves')
    },
    {
      path: '/songs',
      name: 'songs',
      component: require('@/components/Songs')
    },
    {
      path: '/get-data',
      name: 'get-data',
      component: require('@/components/GetData')
    },
    {
      path: '/get-music',
      name: 'get-music',
      component: require('@/components/GetMusic')
    },
    {
      path: '/to-android',
      name: 'to-android',
      component: require('@/components/ToAndroid')
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})
