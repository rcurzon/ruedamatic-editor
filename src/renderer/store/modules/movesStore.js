import DiscDataHelper from '../shared/DiscDataHelper'
import electron from 'electron'

import _sortBy from 'lodash/sortBy'
import _cloneDeep from 'lodash/cloneDeep'

import fs from 'fs'
import path from 'path'
import wget from 'wget-improved'
// import _cloneDeep from 'lodash/cloneDeep'
// import opn from 'opn'
// import yaml from 'js-yaml'
// import util from 'util'

// const HOMEDIR = electron.remote.app.getpath('home')
const DOCDIR = electron.remote.app.getPath('documents')
const TEMPDIR = electron.remote.app.getPath('temp')
const RMDIR = DOCDIR + '/RuedaMaticEditor'

let discDataHelper = new DiscDataHelper()

const state = {
  originalMoves: [], // original object array
  originalCalls: [], // calls in vueltas folder at startup
  editedMoves: [], // edited object, maybe save this
  editedSequence: [], // edited object, maybe save this
  seqFilesModifiedFromMoves: [],
  installedSchemes: [],
  availableSchemes: {},
  basicSchemes: {},
  bUpdateInstalledSchemes: 0,
  schemeName: '',
  schemeProvider: '',
  schemeDate: '',
  showBusy: false
}
/* eslint-disable comma-dangle */
const mutations = {
  FLAG_SEQFILES_TO_RELOAD (state, payload) {
    state.seqFilesModifiedFromMoves = payload
  },
  DELETE_MOVE (state, nDeleteAt) {
    if (nDeleteAt >= 0) {
      let mName = state.editedMoves[nDeleteAt].$.name
      state.editedMoves.splice(nDeleteAt, 1)
      console.log('move deleted:' + mName)
    }
  },
  BUFFER_MOVE_EDIT (state, payload) {
    let editedMove = payload.move
    let nInsertAt = payload.pos
    if (nInsertAt < 0) {
      // not found, it's a new move
      state.editedMoves.splice(-(nInsertAt + 1), 0, editedMove)
    } else {
      // update, replace at the prev version index
      state.editedMoves.splice(nInsertAt, 1, editedMove)
    }
    console.log('move inserted: ' + editedMove.$.name + ', at: ' + nInsertAt)
  },
  ORIGINAL_MOVE_STATE (state, payload) {
    // some diags about calls w/o move, moves w/o call etc.
    let CALLFOLDER = path.join(RMDIR, payload, 'vueltas')
    try {
      let {moves, schemeProvider, schemeDate, schemeName} = discDataHelper.getXMLData(path.join(CALLFOLDER, 'moves.xml'), {type: 'moves'})
      state.schemeName = schemeName
      state.schemeDate = schemeDate
      state.schemeProvider = schemeProvider
      // state.originalMoves.originalMovesModifiedTime = Date.now()
      state.originalMoves = _sortBy(moves, '$.name')
      // WORRY ABOUT CASE? let's not for now: users will need to respect case ...
      let movesNoCall = getters.getMovesNoCall(state)
      let movesTemp = _cloneDeep(state.originalMoves)
      // initialize the edited moves to the original state
      state.editedMoves = movesTemp.map(move => {
        if (movesNoCall.includes(move.$.name)) {
          move._cellVariants = { file: 'danger' }
        } else {
          // console.log('remove cellVariant color flag')
          delete move._cellVariants // no effect if missing property
        }
        return move
      })
    } catch (e) {
      // if (e.code === 'ENOENT')
      state.editedMoves = []
      throw e
    }
  },
  ORIGINAL_CALLS_FILES (state, payload) {
    // these are the filenames of mp3s containing the voice calls
    let scheme = payload
    let CALLFOLDER = path.join(RMDIR, scheme, 'vueltas')
    state.originalCalls = discDataHelper.getCallsHelper(CALLFOLDER)
  },
  SAVE_ALL_MOVES (state, payload) {
    state.editedMoves = _sortBy(state.editedMoves, '$.name')
    let CALLFOLDER = path.join(RMDIR, payload.folder, 'vueltas')
    let asWhat = {
      type: 'moves',
      date: payload.date,
      authorId: payload.authorId,
      schemeName: payload.schemeName,
      schemeProvider: payload.schemeProvider,
      schemeDate: payload.schemeDate
    }
    discDataHelper.saveXMLData(path.join(CALLFOLDER, 'moves.xml'), state.editedMoves, asWhat)
  },
  BUFFER_AVAILABLE_SCHEMES (state, payload) {
    state.availableSchemes = payload
  },
  BUFFER_BASIC_SCHEMES (state, payload) {
    state.basicSchemes = payload
  },
  BUFFER_INSTALLED_SCHEMES (state, payload) {
    state.installedSchemes = payload
  },
  UPDATE_INSTALLED_SCHEMES_FLAG (state, payload) {
    state.bUpdateInstalledSchemes = payload
  },
  SET_BUSY_NOTICE (state) {
    state.showBusy = true
  },
  UNSET_BUSY_NOTICE (state) {
    state.showBusy = false
  }
}

const getters = {
  getMoveByNameObj: (state) => (name) => {
    let nInsertAt = discDataHelper.binarySearch(state.editedMoves, name)
    if (nInsertAt < 0) {
      console.log('Seq move is missing from Move catalog: ' + name.$.name)
      if (name.$.name === 'Continue') {
        throw new Error('Move not found: "' + name.$.name + '"  Every scheme needs a Continue move, 1 measure long, and continue.mp3.  Copy it from the basica scheme.')
      } else {
        throw new Error('Move not found: "' + name.$.name + '"  Was it renamed, deleted?')
      }
    } else {
      // _cloneDeep avoids mutating store in the caller
      return _cloneDeep(state.editedMoves[nInsertAt])
    }
  },
  getMovesNoCall: state => {
    let aryUnmatched = state.originalMoves.reduce(function (unmatched, eachMove) {
      if (state.originalCalls.includes(eachMove.$.file)) {
        // noise: console.log(eachMove.$.file + ' found')
      } else {
        unmatched.push(eachMove.$.name)
      }
      return unmatched
    }
      , [])
    return aryUnmatched
  },
  getMovesNoCallDisplay: (state, getters) => {
    return getters.getMovesNoCall.join(', ') || 'none'
  },
  getCallsUsage: state => {
    return state.originalCalls.map(function (eachCall) {
      return {
        file: eachCall,
        moves: state.editedMoves.filter(move => {
          return move.$.file === eachCall
        })
      }
    })
  },
  getCallsNotUsedDisplay: (state, getters) => {
    let selEntries = getters.getCallsUsage.filter(m => m.moves.length === 0)
    return selEntries.map(ent => ent.file).join(', ') || 'none'
  },

  isCallFile: state => {
    return callerArg => {
      if (state.originalCalls.includes(callerArg)) {
        return true
      } else {
        return false
      }
    }
  }
}

const actions = {
  getSchemes (context, which) {
    context.commit('SET_BUSY_NOTICE')
    // which = available or basic
    if (!['basic', 'available'].includes(which)) {
      throw new Error('getSchemes must be called with one of "available" or "basic"')
    }
    // let that = this
    let fname = 'rme_' + which + '.json'
    let download = wget.download('https://s3.us-east-2.amazonaws.com/come2think.com/RuedaMatic/schemesAndBeats/' + fname, TEMPDIR + '/' + fname, {})
    download.on('error', err => {
      console.log(err)
      throw err
    })
    download.on('end', response => {
      context.commit('UNSET_BUSY_NOTICE')
      console.log('retrieved ' + fname)
      // parse
      try {
        let res = JSON.parse(fs.readFileSync(path.join(TEMPDIR, fname), 'utf8'))
        if (which === 'available') {
          context.commit('BUFFER_AVAILABLE_SCHEMES', res)
        } else {
          context.commit('BUFFER_BASIC_SCHEMES', res)
        }
      } catch (error) {
        console.error(error.stack || error.message || String(error))
        throw error
      }
    }
    )
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
