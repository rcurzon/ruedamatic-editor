import DiscDataHelper from '../shared/DiscDataHelper'
import _cloneDeep from 'lodash/cloneDeep'

// import _sortBy from 'lodash/sortBy'
// import _cloneDeep from 'lodash/cloneDeep'

let discDataHelper = new DiscDataHelper()

// the default assumed name of the data folder
// addtional folders can be set for other calls: suelta, ruedx, wheelchair etc.
const standardRMEfolder = 'scheme_rueda_normal_basica'

const initialSettings = {
  userType: '1', // set calls for a song
  beatsShowUsage: true,
  RMEFolder: standardRMEfolder,
  settingsVersion: 5,
  authorId: process.env.username || process.env.user,
  administrator: false // admin things: button on Sys Info comp to package RM data into Vuepress public folders
}
const state = {
  settings: _cloneDeep(initialSettings),
  version: '2.0.13'
}

// update settings, get a setting
const mutations = {
  CHANGE_RME_FOLDER (state, payload) {
    state.settings.RMEFolder = payload
    discDataHelper.saveUserDataAll(state.settings)
  },
  CHANGE_USERTYPE (state, payload) {
    state.settings.userType = payload
    discDataHelper.saveUserDataAll(state.settings)
  },
  CHANGE_AUTHORID (state, payload) {
    state.settings.authorId = payload
    discDataHelper.saveUserDataAll(state.settings)
  },
  INIT_SETTINGS_DATA (state) {
    if (!discDataHelper.settingsFileExists()) {
      discDataHelper.saveUserDataAll(state.settings)
    } else {
      let settings = discDataHelper.getUserDataAll()
      // overwrite unless it's the exact version we need
      if (settings.settingsVersion !== initialSettings.settingsVersion) {
        discDataHelper.saveUserDataAll(state.settings)
      } else {
        state.settings = settings
      }
    }
  },
  UPGRADE_SETTINGS_DATA (state) {
    if (!discDataHelper.settingsFileExists()) {
      discDataHelper.saveUserDataAll(state)
    }
  },
  UPDATE_SETTINGS_DATA (state, objData) {
    // use to merge a new setting
    Object.assign(state.settings, objData)
    discDataHelper.saveUserDataAll(state.settings)
  }
}

const getters = {
  getUserData (key) {
    // e.g. ...$store.getUserData('version')
    if (!state.settings) {
      mutations.INIT_SETTINGS_DATA(state)
    }
    return state.settings[key]
  }
}

const actions = {
  // testing flexibility of actions
  toggleShowBeatsUsage: (context, val) => { context.commit('UPDATE_SETTINGS_DATA', {beatsShowUsage: val}) }
}

export default {
  state,
  getters,
  mutations,
  actions
}
