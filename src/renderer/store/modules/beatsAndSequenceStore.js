// from https://forum.vuejs.org/t/notify-vue-that-item-has-been-changed-solved/29938/2
// And finally. Since nobody answered, here I found how to tell vue ‘my data structure is changed, please reinitialize observer’.
//
// // Let's say structure of 'item' is changed.
// // We have to give some kick to Vue to reinitialize observer
// // And yes, we need to know 'item' internals :(
//
// // First remove old observer.
// delete this.item._data.__ob__;
//
// // Next define the new one
// Vue.util.defineReactive(this.item.__ob__.value, '_data', this.item._data);
//
// // And finally notify about changes, like $set does
// this.item.__ob__.dep.notify();
// Yes, it is dirty. But it works: https://jsfiddle.net/h34a7s0n/89/

import DiscDataHelper from '../shared/DiscDataHelper'
import Path from 'path'
// import fs from 'fs-extra'
import electron from 'electron'
import Vue from 'vue'
import _cloneDeep from 'lodash/cloneDeep'

const DOCDIR = electron.remote.app.getPath('documents')
const RMDIR = DOCDIR + '/RuedaMaticEditor'

let discDataHelper = new DiscDataHelper()

class BeatTime {
  constructor (time, gear = 'normal', comment = '') {
    this.time = time
    this.gear = gear
    this.comment = comment // free form comment
    this.bsVariant = 'success' // or 'danger', 'warning'
  }
}

const state = {
  lstTimesOrig: [],
  lstTimesWork: [],
  originalSeq: [], // original object array
  editedSeq: [], // edited object, optionally save this,
  MP3FileName: '', // use getter/computed for XML, SEQ filenames
  MP3FileNameMd5GivenXML: '', // md5 expected according to loaded BEATS (.xml) file
  MP3FileNameMd5GivenSEQ: '', // md5 expected according to loaded SEQUENCE (.seq) file
  MP3FileNameMd5Actual: '',
  xmlAuthor: '',
  xmlDate: 0,
  MP3URL: '',
  seqAuthor: '',
  seqAuthorDate: 0,
  seqMd5: '',
  seqFile: '',
  seqScheme: '',
  seqSchemeAuthor: '',
  seqSchemeDate: 0,
  bReloadCurrentSong: false // for auto renaming of moves in seq files, when a move name is changed on Moves tab
}

const mutations = {
  NUDGE_ALL_CALLS (state, payload) {
    // payload: index, count.  If no index
    //  then consider start as 0
    let index = payload.index || 0
    let count = payload.count
    if (!payload.index) {
      if (count < 0) {
        state.editedSeq.splice(index, -count)
      } if (count > 0) {
        state.editedSeq.splice(index, 0, ...new Array(count)) // increase seq - which is a sparse array
        // trim if it's now longer than the beats (times) array
        state.editedSeq.splice(state.lstTimesWork.length) // if it's become longer than beats, trim it!
      }
    }
  },
  DELETE_ALL_CALLS (state, payload) {
    state.editedSeq = []
  },
  NUDGE_ALL_BEATS (state, seconds) {
    let lstTimesLocal = state.lstTimesWork.map(beat => {
      beat.time += seconds
      return beat
    })
    state.lstTimesWork = lstTimesLocal
  },
  SET_RELOAD_CURRENT_SONG (state) {
    state.bReloadCurrentSong = true
  },
  UNSET_RELOAD_CURRENT_SONG (state) {
    state.bReloadCurrentSong = false
  },
  SET_MP3NAME_AND_DEPS (state, payload) {
    try {
      if (payload.MP3FileName === '') {
        state.MP3FileName = ''
        state.MP3FileNameMd5GivenXML = ''
        state.MP3FileNameMd5GivenSEQ = ''
        state.MP3FileNameMd5Actual = ''

        state.xmlAuthor = ''
        state.xmlDate = 0
        state.MP3URL = ''

        state.seqAuthor = ''
        state.seqAuthorDate = 0
        state.MP3FileNameMd5GivenSEQ = ''
        state.editedSeq = ''
        state.seqFile = ''
        state.seqAuthor = ''
        state.seqScheme = ''
        state.seqSchemeAuthor = ''
        state.seqSchemeDate = 0
      } else {
        if (payload.MP3FileName) state.MP3FileName = payload.MP3FileName
        if (payload.MP3FileNameMd5GivenXML) state.MP3FileNameMd5GivenXML = payload.MP3FileNameMd5GivenXML
        if (payload.MP3FileNameMd5GivenSEQ) state.MP3FileNameMd5GivenSEQ = payload.MP3FileNameMd5GivenSEQ
        if (payload.MP3FileNameMd5Actual) state.MP3FileNameMd5Actual = payload.MP3FileNameMd5Actual

        if (payload.xmlAuthor) state.xmlAuthor = payload.xmlAuthor
        if (payload.xmlDate) state.xmlDate = payload.xmlDate
        if (payload.MP3URL) state.MP3URL = payload.MP3URL

        if (payload.seqAuthor) state.seqAuthor = payload.seqAuthor
        if (payload.seqAuthorDate) state.seqAuthorDate = payload.seqAuthorDate
        if (payload.MP3FileNameMd5GivenSEQ) state.MP3FileNameMd5GivenSEQ = payload.MP3FileNameMd5GivenSEQ
        if (payload.seqFile) state.seqFile = payload.seqFile
        if (payload.seqAuthor) state.seqAuthor = payload.seqAuthor
        if (payload.seqScheme) state.seqScheme = payload.seqScheme
        if (payload.seqSchemeAuthor) state.seqSchemeAuthor = payload.seqSchemeAuthor
        if (payload.seqSchemeDate) state.seqSchemeDate = payload.seqSchemeDate
      }
    } catch (e) {
      console.log('error setting MP3 and HASH for: ' + state.MP3FileName)
      throw e
    }
  },
  SET_BEATS_AUTHOR_AND_URL (state, payload) {
    try {
      state.xmlAuthor = payload.authorId
      state.xmlDate = payload.beatsAuhtorDate
      state.MP3URL = payload.MP3URL
    } catch (e) {
      console.log('error setting Beats Author INFO for: ' + state.MP3FileName)
      throw e
    }
  },
  INIT_BEATS (state, beats) {
    state.lstTimesOrig = beats
    state.lstTimesWork = beats
  },
  SAVE_BEATS (state, asWhat) {
    state.lstTimesWork = asWhat.lstTimes
    discDataHelper.saveXMLData(
      getters.getXMLFileName(state)(asWhat.RMEFolder),
      asWhat.lstTimes,
      asWhat
    )
  },
  SET_BEAT_WARNING (state, payload) {
    Vue.set(state.lstTimesWork[payload.ind], 'bsVariant', payload.variant)
  },
  SET_BEAT_BOING (state, payload) {
    if (payload.elem) {
      Vue.set(payload.elem, 'boing', payload.boing)
    } else if (payload.ind) {
      Vue.set(state.lstTimesWork[payload.ind], 'boing', payload.boing)
    }
  },
  CLEAR_BEAT_BOINGS (state) {
    state.lstTimesWork.forEach((elem, ind) => {
      Vue.set(elem, 'boing', false)
    })
  },
  ADD_NEW_BEAT (state, time) {
    state.lstTimesWork.push(time) // quote: Vue wraps array methods like push, splice etc so they will also trigger view updates.
  },
  DELETE_ONE_BEAT (state, index) {
    state.lstTimesWork.splice(index, 1) // quote: Vue wraps array methods like push, splice etc so they will also trigger view updates.
  },
  DELETE_BEATS_AFTER (state, index) {
    state.lstTimesWork.splice(index, state.lstTimesWork.length) // quote: Vue wraps array methods like push, splice etc so they will also trigger view updates.
  },
  INSERT_BEAT_BEFORE (state, payload) {
    let index = payload.index
    if (index > 0) {
      let time = (state.lstTimesWork[index].time + state.lstTimesWork[index - 1].time) / 2
      let bt = new BeatTime(time)
      state.lstTimesWork.splice(index, 0, bt) // quote: Vue wraps array methods like push, splice etc so they will also trigger view updates.
    } else {
      // use the mean: field
      let earlyTime = state.lstTimesWork[index].time - payload.mean
      if (earlyTime > 0) {
        let bt = new BeatTime(earlyTime)
        state.lstTimesWork.splice(0, 0, bt) // quote: Vue wraps array methods like push, splice etc so they will also trigger view updates.
      }
    }
  },
  ADJUST_BEAT_TIME (state, payload) {
    state.lstTimesWork[payload.ind] = payload.time
  },
  SAVE_SEQ (state, asWhat) {
    discDataHelper.saveXMLData(
      getters.getSEQFileName(state)(asWhat.RMEFolder),
      asWhat.lstSeq,
      asWhat
    )
  },
  LOAD_BEATS (state, lstTimes) {
    state.lstTimesWork = lstTimes
  },
  LOAD_SEQ (state, seqSaved) {
    state.editedSeq = seqSaved
  },
  DELETE_THIS_SEQ_MOVE (state, {index, lstTimes}) {
    let currMoveName = state.editedSeq[index].$.name
    state.editedSeq.splice(index, 1, undefined) // Vue $delete is not js delete, and js delete is not reactive
    for (var i = 1; index + i < lstTimes.length; i++) {
      if (state.editedSeq[index + i] &&
          state.editedSeq[index + i].$.name === currMoveName &&
          state.editedSeq[index + i].$.length === 0) {
        Vue.set(state.editedSeq, index + i, undefined) // Vue $delete is not js delete, js delete is not reactive
      } else {
        break
      }
    }
  },
  ADD_THIS_SEQ_MOVE (state, {index, item, maxBeat}) {
    if (state.editedSeq[index]) {
      // clear the old move and it's continuations... eventually s/b redundant
      for (var i = 1; index + i < maxBeat; i++) {
        if (state.editedSeq[index + i] &&
            state.editedSeq[index + i].$.name === state.editedSeq[index].$.name &&
            state.editedSeq[index + i].$.length === 0) {
          Vue.set(state.editedSeq, index + i, undefined) // Vue $delete is not js delete, js delete is not reactive // Vue $delete is not js delete, js delete is not reactive
        } else {
          break
        }
      }
    }
    // set the new call...
    Vue.set(state.editedSeq, index, item)
    // and it's continuations
    for (var j = 1; j < item.$.length; j++) {
      if (index + j < maxBeat) { // ???
        Vue.set(state.editedSeq, index + j, _cloneDeep(item)) // clone or base move changes too
        // zero length: marks that it is a continuation of a move
        state.editedSeq[index + j].$.length = 0
      }
    }
  }
}

const getters = {
  getXMLFileName: state => (folder) => {
    let beatsFolder = Path.join(RMDIR, 'compases_para_canciones')
    if (state.MP3FileName) {
      return Path.join(
        beatsFolder,
        Path.basename(state.MP3FileName, Path.extname(state.MP3FileName)) +
        '.xml'
      )
    } else {
      // clicking on Vuex in devtools may cause this to be evaluated before init'd
      return undefined
    }
  },
  getFileMd5Given: state => {
    return state.MP3FileNameMd5Given
  },
  getFileMd5Actual: state => {
    return state.MP3FileNameMd5Actual
  },
  // 300818:1701 not used
  getSEQFileName: state => (folder) => {
    let BEATSFOLDER = Path.join(RMDIR, folder, 'secuencias_para_canciones')
    if (state.MP3FileName) {
      return Path.join(
        BEATSFOLDER,
        Path.basename(state.MP3FileName, Path.extname(state.MP3FileName)) +
        '.seq'
      )
    } else {
      // clicking on Vuex in devtools may cause this to be evaluated before init'd
      return undefined
    }
  }
}

const actions = {
  setMP3NameAndCalcMd5 (context, payload) {
    // assume that caller has ENSURED the MP3 file exists
    context.commit('SET_MP3NAME_AND_DEPS', payload)
    try {
      electron.ipcRenderer.on('asynch-md5-result', (event, data) => {
        console.log('ipcRenderer got event with: ' + data)
        context.commit('SET_MP3NAME_AND_DEPS', {MP3FileName: payload.MP3FileName, MP3FileNameMd5Actual: data})
      })
      console.log('ipcRenderer requesting md5')
      electron.ipcRenderer.send('asynch-md5-request', payload.MP3FileName)
    } catch (e) {
      console.log('error setting MP3 and HASH for: ' + payload.MP3FileName)
      throw e
    }
  }
}

export default {
  state,
  getters,
  mutations,
  actions
}
