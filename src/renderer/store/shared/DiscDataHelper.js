/**
 * Created by rc on 6/18/17.
 */

/* discDataHelper js: it's gonna let us edit the moves, sequences,
 call files... also, diagnose dependencies between these.
 */

/*
  ORIGINAL JSON from moves.xml
  JSON.stringify(moves,null ,2) produces:
{
  'root': {
  'moves': [
    {
      'move': [
        {
          '$': {
            'name': 'Abajo',
            'file': 'abajo.mp3',
            'length': '1',
            'lengthfraction': '',
            'delaycount': ''
          },
          'comment': [
            'man walks backwards'
          ]
        },
        {
          '$': {
            'name': 'Abanico',
            'file': 'abanico.mp3',
            'length': '4',
            'lengthfraction': '',
            'delaycount': ''
          },
          'comment': [
            'eew'
          ]
        },
        {
          '$': {

IDEA: on saving edit, make it back into an array.
  Can use _.values(obj)
     It looks like you want to diff keys (or rather, it'd be efficient to — _.keys)

     _.difference(
       _.keys({104: 1, 102: 3, 101: 0}),  ['104', '102', '101']
       _.keys({104: 1, 102: 3})  ['104', '102']
     )
  [ '101' ]
     Or, you could always convert your object to an array of pairs if you want to compare within the objects too (_.pairs):

     _.difference(
       _.pairs({104: 1, 102: 3, 101: 0}),  [['104',1], ['102',3], ['101',0]]
       _.pairs({104: 1, 102: 2})  [['104',1], ['102',2]]
     )
  [['102', 3], ['101', 0]]
 */
//  npm installed, Aug 1 2018 rc
// xml2js notes:  https://stackoverflow.com/questions/20238493/xml2js-how-is-the-output#21977456
import fs from 'fs-extra'
import xml2js from 'xml2js'
import _ from 'lodash'
import electron from 'electron'
import path from 'path'
import _cloneDeep from 'lodash/cloneDeep'
import wget from 'wget-improved'
import AdmZip from 'adm-zip'

// import {exec} from 'child-process'
// endregion
const DOCDIR = electron.remote.app.getPath('documents')
const TEMPDIR = electron.remote.app.getPath('temp')
const HOMEDIR = electron.remote.app.getPath('home')
/* eslint-disable no-unused-vars */
const RMDIR = DOCDIR + '/RuedaMaticEditor'

class discDataHelper {
  // /////////////////////////////
  // util functions in this part: call from anywhere
  // (just saves importing fs wherever needed)

  getTimeStampString () {
  //  TODO: gen name for generated scheme folders and seq files
  //  https://stackoverflow.com/questions/50182734/grunt-cssmin-and-timestamp-filename
  // Obtain local timestamp formatted as: YYYY-MM-DD-HH-MM-SS
    var tzOffset = (new Date()).getTimezoneOffset() * 60000
    var timeStamp = (new Date(Date.now() - tzOffset)).toISOString().slice(0, -1)
      .replace(/\.[\w\W]+?$/, '') // Delete from dot to end.
      .replace(/:|\s|T/g, '-') // Replace colons, spaces, and T with hyphen.
    return timeStamp
  }

  checkForSeqChanges (path1, path2) {
    // TODO: USE ON SEQ UPDATE
    // here's some code to get here
    // let ddh = new DiscDataHelper()
    // ddh.checkForMovChanges()
    // console.log(ddh)

    //  USERS can assume that any new version of the SAME SCHEME
    //   is going to be valid with their old SEQ files.
    // However, if we have a SEQ for a given song already, find if contents are identical
    // If YES, leave the old one - DONE
    // If NO, present dialog: overwrite, use new, save both?
    // so the plan:
    // 1. get the currently installed SEQ
    // 2. get the incoming SEQ
    // 3. both: extract just the <move> lines
    // 4. run the diff
    // 5. if any '-' or '+' lines - a diff seq
    let file1 = fs.readFileSync('C:\\Users\\rc\\temp\\moves.xml', 'utf8')
    let file2 = fs.readFileSync('C:\\Users\\rc\\temp\\moves2.xml', 'utf8')
    let d = require('diff').createTwoFilesPatch('C:\\Users\\rc\\temp\\moves.xml', 'C:\\Users\\rc\\temp\\moves2.xml', file1, file2, 'yy', 'zz', {context: 2})
    console.log(d)
  }

  checkForMovChanges (path1, path2) {
    // TODO: USE ON SCHEME UPDATE
    //  USERS can assume that any new version of the SAME SCHEME
    //   is going to be valid with their old SEQ files.
    // However, if we have a SEQ for a given song already, find if contents are identical
    // If YES, leave the old one - DONE
    // If NO, present dialog: overwrite, use new, save both?
    // so the plan:
    // 1. get the currently installed SEQ
    // 2. get the incoming SEQ
    // 3. both: extract just the <move> lines
    // 4. run the diff
    // 5. if any '-' or '+' lines - a diff seq
    // just the moves
    let file1 = fs.readFileSync('C:\\Users\\rc\\temp\\moves.xml', 'utf8')
    let re = /(?:.*\n)*(.*<moves>(?:.*\n)*.*<\/moves>.*\n)(?:.*\n)*.*/g
    let sMoves = file1.replace(re, '$1')
    // replace <comment>.*<\/comment>
    // with <comment/>
    let file2 = fs.readFileSync('C:\\Users\\rc\\temp\\moves2.xml', 'utf8')
    let d = require('diff').createTwoFilesPatch('C:\\Users\\rc\\temp\\moves.xml', 'C:\\Users\\rc\\temp\\moves2.xml', file1, file2, 'yy', 'zz', {context: 2})
    console.log(d)
  }

  getScheme (sScheme, callback) {
    let fname = sScheme
    let download = wget.download('https://s3.us-east-2.amazonaws.com/come2think.com/RuedaMatic/schemesAndBeats/' + fname, TEMPDIR + '/' + fname, {})
    download.on('error', err => {
      console.log(err)
    })
    download.on('end', response => {
      try {
        let zip = new AdmZip(TEMPDIR + '/' + fname)
        try {
          // if we add more samples, this will have to be "getEntries" returning an array
          // to iterate, testing for Entry name starts with Music/
          let music = zip.getEntry('Music/RM Sample.mp3')
          // bools: maintainEntryPath, overwrite
          zip.extractEntryTo(music, HOMEDIR, true, true)
        } catch (e) {
          console.log('no Music folder content')
        }
        // now we fish in the folders that admzip does not call "Directories"... so they have no entry
        let res = zip.getEntries()

        let beats = res.filter(ent => ent.entryName.startsWith('compases_para_canciones'))
        beats.forEach(item => {
          // bools: maintainEntryPath, overwrite
          zip.extractEntryTo(item, RMDIR, true, true)
        })

        let schemeEntries = res.filter(ent => ent.entryName.startsWith('scheme_'))
        schemeEntries.forEach(item => {
          // bools: maintainEntryPath, overwrite
          zip.extractEntryTo(item, RMDIR, true, true)
        })

        callback()
      } catch (error) {
        console.error(error.stack || error.message || String(error))
        throw error
      }
    })
  }

  ensureSchemeFoldersExist (scheme) {
    try {
      fs.ensureDir(RMDIR + '/' + scheme + '/vueltas')
      fs.ensureDir(RMDIR + '/' + scheme + '/secuencias_para_canciones')
    } catch (e) {
      console.log(e)
      throw e
    }
  }
  fileExists (rFile) {
    return !!fs.existsSync(rFile)
  }

  // /////////////////////////////
  // store functions in this part: only call from store
  getUserData (sDataItem) {
    let uData = this.getUserDataAll()
    return uData[sDataItem]
  }

  getUserDataAll () {
    let uDataDir = electron.remote.app.getPath('userData')
    // note: in production,    'userData' = C:\Users\rc\AppData\Roaming\ruedamatic-editor\
    // but:  in development,   'userData' = C:\Users\rc\AppData\Roaming\Electron\
    let udFile = path.join(uDataDir, 'rme', 'userData.json')
    let obj = JSON.parse(fs.readFileSync(udFile, 'utf8'))
    return obj
  }

  saveUserDataAll (objData) {
    let uDataDir = electron.remote.app.getPath('userData')
    let appConfFolder = path.join(uDataDir, 'rme')
    fs.ensureDirSync(appConfFolder)
    let udFile = path.join(appConfFolder, 'userData.json')
    fs.writeFileSync(udFile, JSON.stringify(objData, null, 2), 'utf8')
  }

  settingsFileExists () {
    let uDataDir = electron.remote.app.getPath('userData')
    let udFile = path.join(uDataDir, 'rme', 'userData.json')
    return this.fileExists(udFile)
  }

  getCallsHelper (folder) {
    try {
      let callAry = fs.readdirSync(folder)
      return callAry.filter(fname => path.extname(fname) === '.mp3' ||
        path.extname(fname) === 'm4a' ||
        path.extname(fname) === '.3gp')
    } catch (e) {
      // directories should exist due to ensureSchemeFoldersExist
      console.log(e)
    }
  }

  getXMLData (inFile, asWhat) {
    let parser = new xml2js.Parser()
    let fileData = null
    let xmlContent = null

    try {
      fileData = fs.readFileSync(inFile)
    } catch (ex) {
      console.log('Does file exist? Unable to read file \'' + inFile + '\'.')
      throw ex
      // console.log(ex)
    }
    /* eslint-disable indent */ // BEGIN: es-lint can't understand callback indent
    parser.parseString(fileData, function (err, result) {
      if (err) {
        console.log('Unable to parse file \'' + inFile + '\'.')
        console.log(err)
        throw err
      } else {
        console.log('The file \'' + inFile + '\' was parsed!')
        xmlContent = result
      }
    })
    /* eslint-enable indent */ // END: es-lint can't understand callback indent
    if (asWhat.type === 'moves') {
      // sort by name
      let moves = _.sortBy(xmlContent.root.moves[0].move, '$.name')
      let testmoves = moves.reduce(function (acc, cur, i) {
        if (_.has(acc, cur.$.name)) {
          console.log('ERR: Move name not unique! ' + cur.$.name)
        }
        if (_.trim(cur.$.name).length < 3) {
          console.log('ERR: Move name illegal, too short! ' + cur.$.name)
        }
        acc[cur.$.name] = cur
        return acc
      }, {}) // initial value is an empty object
      return {
        moves: _.values(testmoves),
        schemeProvider: (xmlContent.root.author && xmlContent.root.author[0].$.authorId) || '',
        schemeDate: (xmlContent.root.author && xmlContent.root.author[0].$.date) || '',
        schemeName: (xmlContent.root.scheme && xmlContent.root.scheme[0].$.schemeName) || ''}
    } else if (asWhat.type === 'beats') {
      // note return is an object now!
      // following: copy of the Beats class for convenience
      class BeatTime {
        constructor (time, gear = 'normal', comment = '') {
          this.time = time
          this.gear = gear
          this.comment = comment // free form comment
          this.bsVariant = 'success' // or 'danger', 'warning'
        }
      }
      return {
        beats: xmlContent.root.beats[0].beat.map(beat => new BeatTime(beat._ / 1000, beat.$.gear || 'normal', beat.$.comment || '')),
        md5: xmlContent.root.musicfile ? xmlContent.root.musicfile[0].$.md5 : 0,
        authorId: xmlContent.root.authorAndSongURL[0].$.authorId,
        authorDate: xmlContent.root.authorAndSongURL[0].$.date,
        MP3URL: xmlContent.root.authorAndSongURL[0]._
      }
    } else if (asWhat.type === 'sequence') {
      // STRUCT: xmlContent.sequences.sequence[0].move[0].$.name
      return xmlContent.root
      // due to xml2js, each is object.  with obj.$.name.  Binary search takes the whole object
    } else { // just the data ma'am
      console.log('SEQ file contains non-standard data!')
      return xmlContent
    }
  }

  saveXMLData (outFilePath, objData, asWhat) {
    // COMMENT: since we save at every change now, drop these backups
    // // save 3 generations of backup .bak.1 to .bak.3
    // // asWhat, parsed diff
    // if (fs.exists(outFilePath + '.bak.2')) {
    //   try {
    //     fs.moveSync(outFilePath + '.bak.2', outFilePath + '.bak.3', {
    //       overwrite: true
    //     })
    //   } catch (error) {
    //     console.log('error backing up measures file bak.2 to bak.3 - does source exist?')
    //   }
    // }
    // // backup to .2
    // if (fs.exists(outFilePath + '.bak.1')) {
    //   try {
    //     fs.moveSync(outFilePath + '.bak.1', outFilePath + '.bak.2', {
    //       overwrite: true
    //     })
    //   } catch (error) {
    //     console.log('error backing up measures file bak.1 to bak.2 - does source exist?')
    //   }
    // }
    // // backup to .1
    // if (fs.exists(outFilePath)) {
    //   try {
    //     fs.moveSync(outFilePath, outFilePath + '.bak.1', {
    //       overwrite: true
    //     })
    //   } catch (error) {
    //     console.log('error backing up measures file to .bak.1 - does source exist?')
    //   }
    // }

    // first clear the embedded GUI flags from the domain data (non-existent CALL file warning, cell is RED)
    let objDataClean = []
    if (Array.isArray(objData)) {
      if (asWhat.type === 'moves') {
        objDataClean = objData.map(item => {
          const temp = _cloneDeep(item)
          delete temp._cellVariants
          delete temp.$.howl
          return temp
        })
      } else if (asWhat.type === 'beats') {
        objDataClean = objData.map(item => {
          return {
            _: Math.round(1000 * item.time),
            $: { gear: item.gear, comment: item.comment }
          }
        })
      } else if (asWhat.type === 'sequence') {
        let countExtended = 0
        for (let i = 0; i < objData.length; i++) {
          if (objData[i]) {
            if (objData[i].$ && objData[i].$.length === 0) {
            // a extended measure (continuation): these are dropped
              countExtended += 1
            } else {
              objDataClean[i - countExtended] = {$: {name: objData[i].$.name}}
            }
          } else {
            objDataClean[i - countExtended] = {$: {name: 'Continue'}}
          }
        }
      }
    } else {
      objDataClean = objData
    }
    // now write the file via xml2js
    let builder = new xml2js.Builder()
    let xml = null
    if (asWhat.type === 'moves') {
      xml = builder.buildObject({
        root: [
          {author: {
            '$': {
              date: asWhat.date,
              authorId: asWhat.authorId
            },
            '_': 'empty'
          }},
          {scheme: {
            '$': {
              schemeName: asWhat.schemeName,
              schemeProvider: asWhat.schemeProvider,
              schemeDate: asWhat.schemeDate
            },
            '_': 'empty'
          }},
          {
            moves: {
              move: objDataClean
            }
          }
        ]
      })
    } else if (asWhat.type === 'beats') {
      // note the Beat object is replaced with a millisecond integer in the "clean" above
      xml = builder.buildObject({
        root: [
          {musicfile: {
            '$': {
              md5: asWhat.md5
            },
            '_': path.basename(asWhat.filename)
          }},
          {authorAndSongURL: {
            '$': {
              date: asWhat.date,
              authorId: asWhat.authorId
            },
            '_': asWhat.MP3URL
          }},
          // {format: {
          //   '_': '2.0'
          // }},
          {beats: {
            beat: objDataClean
          }}
        ]
      })
    } else if (asWhat.type === 'sequence') {
      xml = builder.buildObject({
        root: [
          {musicfile: {
            '$': {
              md5: asWhat.md5
            },
            '_': path.basename(asWhat.filename)
          }},
          {scheme: {
            '$': {
              provider: asWhat.schemeProvider,
              date: asWhat.schemeDate
            },
            '_': asWhat.schemeName
          }},
          {author: {
            '$': {
              authorId: asWhat.authorId,
              date: asWhat.authorDate
            },
            '_': 'empty'
          }},
          {sequences:
            { sequence:
              [ { '$': { id: '1', name: 'standard', type: 'shared' },
                move: objDataClean
              } ]
            }
          }
        ]
      })
    } else if (asWhat.type === 'plain') {
      xml = builder.buildObject(objDataClean)
    }
    // could be writeFileSync but may not be required, s/b more responsive this way
    fs.writeFile(outFilePath, xml, function (err) {
      if (err) {
        throw err
      }
      console.log('The file ' + outFilePath + ' was saved!')
    })
  }

  // http://jsfiddle.net/aryzhov/pkfst550/
  /*
      * Binary search in JavaScript.
      * Returns the index of of the element in a sorted array or (-n-1) where n is the insertion point for the new element.
      * Parameters:
      *     ar - A sorted array
      *     el - An element to search for
      *     RC: the compareFunc is the only place I customize for data specifics, plus I embedded it
      *     compareFunc - A comparator function. The function takes two arguments: (a, b) and returns:
      *        a negative number  if a is less than b
      *        0 if a is equal to b
      *        a positive number of a is greater than b.
      * The array may contain duplicate elements. If there are more than one equal elements in the array,
      * the returned value can be the index of any one of the equal elements.
      */
  /* esl-disable no-unused-vars */
  binarySearch (ar, el) {
    function compareFunc (a, b) {
      // return a - b
      if (a.$) {
        if (a.$.name === b.$.name) {
          return 0
        } else if (a.$.name > b.$.name) {
          return +1
        } else if (a.$.name < b.$.name) {
          return -1
        }
      } else {
        if (a === b) {
          return 0
        } else if (a > b) {
          return +1
        } else if (a < b) {
          return -1
        }
      }
    }
    var m = 0
    var n = ar.length - 1
    while (m <= n) {
      var k = (n + m) >> 1
      var cmp = compareFunc(el, ar[k])
      if (cmp > 0) {
        m = k + 1
      } else if (cmp < 0) {
        n = k - 1
      } else {
        return k
      }
    }
    return -m - 1
  }
}

export default discDataHelper
